# Chapter 1. Introduction

Here you will find a script called
`spekpy demo.py`. This script demonstrates how to use the SpekPy toolkit to
generate and plot an x-ray spectrum and how to calculate half-value layers,
air kerma and fluence. Make sure you install SpekPy-v2 (version 2) before
trying to run it. See: [https://bitbucket.org/spekpy/spekpy_release](https://bitbucket.org/spekpy/spekpy_release).

Prerequisites:

* Python 3
* NumPy (standard Python library)
* SciPy (standard Python library)
* matplotlib (standard Python library)
* SpekPy V2 (our custom Python library)

