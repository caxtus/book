# Chapter 2. Basics of x-ray tubes

The Python script used to generate fig. 2.3 is available here. The script is
called `heel_effect.py` and uses the SpekPy-v2 toolkit (See: [https://bitbucket.org/spekpy/spekpy_release](https://bitbucket.org/spekpy/spekpy_release)). Try changing the beam
filtration in the script and running it. In the region where both beams are
non-zero, do the beam profiles become more or less similar as the filtration
is increased? Can you explain this?.

Prerequisites:

* Python 3
* NumPy (standard Python library)
* SciPy (standard Python library)
* matplotlib (standard Python library)
* SpekPy V2 (our custom Python library)

