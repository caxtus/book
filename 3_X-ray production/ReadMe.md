# Chapter 3. X-ray production

A supplementary Python script to this chapter is available here. The script is
called `percent_char.py` and uses the SpekPy-v2 toolkit (See: [https://bitbucket.org/spekpy/spekpy_release](https://bitbucket.org/spekpy/spekpy_release)); it plots the percentage
contribution of characteristic radiation to the total fluence as a function of
tube potential. The tube filtration is set to 3 mm Be (inherent), 0.208 mm
Al (added) and 500 mm of air (exit window to detector). Try increasing the
filtration of aluminium to 1 mm. How do the results change and why?

Prerequisites:

* Python 3
* NumPy (standard Python library)
* SciPy (standard Python library)
* matplotlib (standard Python library)
* SpekPy V2 (our custom Python library)

