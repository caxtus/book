# Chapter 4. Basic dosimetry and beam-quality characterization

A supplementary Python script to this chapter is available here. The script is
called `mean coeff.py` and uses the SpekPy-v2 toolkit (See: [https://bitbucket.org/spekpy/spekpy_release](https://bitbucket.org/spekpy/spekpy_release)); it reproduces the data
of table 4.1. Try increasing the aluminium filtration to 1 mm and running
the script. Do the coefficients calculated via the fluence, energy-fluence and
kerma spectra agree more or less closely after the increase in filtration? Can
you explain this? Note that the script also prints out the homogeneity index.

Prerequisites:

* Python 3
* NumPy (standard Python library)
* SciPy (standard Python library)
* SpekPy V2 (our custom Python library)

