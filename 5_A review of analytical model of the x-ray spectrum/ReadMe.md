# Chapter 5. A review of analytical models of the x-ray spectrum

The Python script used for generating figs. 5.1 and 5.2 is available here. The script is called
`classical_approx.py` and uses the SpekPy-v2 toolkit (See: [https://bitbucket.org/spekpy/spekpy_release](https://bitbucket.org/spekpy/spekpy_release)). Try running the script
for tube potentials intermediate between 35 and 100 kV. Do you agree with
the statement at the end of Section 5.1 that the classical cross section of
Kramers loses validity by 50 kV?

Prerequisites:

* Python 3
* NumPy (standard Python library)
* SciPy (standard Python library)
* matplotlib (standard Python library)
* SpekPy V2 (our custom Python library)

