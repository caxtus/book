# Calculating X-ray Tube Spectra: Analytical and Monte Carlo Approaches (OR CaXTuS)

## By Gavin Poludniowski, Artur Omar, and Pedro Andreo 

## (CRC Press, 2022)

<br>
[Link](https://doi.org/10.1201/9781003058168)

## Abstract

Calculating x-ray tube spectra provides a comprehensive review of the modelling of x-ray tube emissions, with a focus on medical imaging and radiotherapy applications. It begins by covering the relevant background, before discussing modelling approaches, including both analytical formulations and Monte Carlo simulation. Historical context is provided, based on the past century of literature, as well as a summary of recent developments and insights. The book finishes with example applications for spectrum models, including beam quality prediction and the calculation of dosimetric and image-quality metrics.

This book will be a valuable resource for postgraduate and advanced undergraduate students studying medical radiation physics, in addition to those in teaching, research, industry and healthcare settings whose work involves x-ray tubes.

## Key Features

* Covers simple modelling approaches as well as full Monte Carlo simulation of x-ray tubes
* Bremsstrahlung and characteristic contributions to the spectrum are discussed in detail
* Learning is supported by free open-source software and an online repository of code

## Table-of-contents

Chapter 1. Introduction.

Chapter 2. Basics of X-ray tubes.

Chapter 3. X-ray Production

Chapter 4. Basic Dosimetry and Beam-quality Characterization.

Chapter 5. A review of Analytical Models for the X-ray Spectrum

Chapter 6. An Analytical Model in Detail.

Chapter 7. Overview on Monte Carlo Modelling.

Chapter 8. Predicting and Matching Half-value Layers.

Chapter 9. Kilovoltage X-ray Beam Dosimetry.

Chapter 10. Optimizing the Tube Potential

## Accompanying Software

Python scripts to support learning from this book are available here in this online
repository. Make sure you install SpekPy (version 2) before trying to run it. SpekPy is a toolkit for modelling x-ray tube spectra in the Python programming language. It can be used to calculate on- and off-axis spectra for tungsten, molybdenum, and rhodium anodes. The toolkit is distributed free-of-charge under the open-source MIT license. Find out more here: [https://bitbucket.org/spekpy/spekpy_release](https://bitbucket.org/spekpy/spekpy_release).

The scripts have been tested with SpekPy V2.07 and Python 3.8.8 (via Anaconda) on a Linux system.

## License

The Python (and other) scripts in this repository are made available free-of-charge under the MIT license. Please see the license file. The book cover image (cover_caxtus.jpg) is excluded from this licensing.

## Contact

For comments, queries or to report bugs, you can raise an issue on Bitbucket for this repository or email Gavin Poludniowski (gpoludniowski@gmail.com). We hope that you find this repository useful!

<br>
![cover](cover_caxtus.jpg){width=50%}
